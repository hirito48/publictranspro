//
//  Date.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 7/29/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import Foundation
import UIKit

extension Date {

    func convertToView()->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        return formatter.string(from: self)
    }
}
