//
//  JSON.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 7/9/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import Foundation
import SwiftyJSON

extension JSON{
    mutating func appendIfArray(json:JSON){
        if var arr = self.array{
            arr.append(json)
            self = JSON(arr)
        }
    }
}
