//
//  TravelViewController.swift
//  TransPro
//
//  Created by Hirito48 on 2/12/17.
//  Copyright © 2017 Hirito48. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class TravelViewController: UIViewController {
    
    @IBOutlet weak var City1Button: UIButton!
    @IBOutlet weak var City2Button: UIButton!
    @IBOutlet weak var DateButton: UIButton!
    @IBOutlet weak var TimeButton: UIButton!
    @IBOutlet weak var OriginLabel: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    @IBOutlet weak var DepartureLabel: UILabel!
    @IBOutlet weak var DestinationLabel: UILabel!
    
    var DeparturePicker = Launcher(type: "departure")
    var DatePicker = DatePickerLauncher()
    var OriginCityPicker = Launcher(type: "city", id: "origin")
    var DestinationCityPicker = Launcher(type: "city", id: "destination")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DeparturePicker.delegate = self
        DatePicker.delegate = self
        OriginCityPicker.delegate = self
        DestinationCityPicker.delegate = self
        
        TimeButton.addTarget(DeparturePicker, action: #selector(DeparturePicker.handlePresent), for: .touchUpInside)
        City1Button.addTarget(OriginCityPicker, action: #selector(OriginCityPicker.handlePresent), for: .touchUpInside)
        City2Button.addTarget(DestinationCityPicker, action: #selector(DestinationCityPicker.handlePresent), for: .touchUpInside)
        DateButton.addTarget(DatePicker, action: #selector(DatePicker.handlePresent), for: .touchUpInside)
        
    
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        City1Button.layer.addBorder(edge: .bottom, color: Helper().TransProColor(color : "Black", alpha: 0.4), thickness: 0.5)
        City2Button.layer.addBorder(edge: .bottom, color: Helper().TransProColor(color : "Black", alpha: 0.4), thickness: 0.5)
        DateButton.layer.addBorder(edge: .bottom, color: Helper().TransProColor(color : "Black", alpha: 0.4), thickness: 0.5)
        TimeButton.layer.addBorder(edge: .bottom, color: Helper().TransProColor(color : "Black", alpha: 0.4), thickness: 0.5)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TravelToLocation" {
//            let dest = segue.destination as! LocationViewController
//            dest.previousController = "Travel"
        }
    }
    
    func openLocation(){
        performSegue(withIdentifier: "TravelToLocation", sender: nil)
    }
    
    @IBAction func BackButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func SubmitButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "TravelToResult", sender: sender)
    }
}

extension TravelViewController : LauncherDelegate {
    func didSelectedLauncher(type: String, item: JSON) {
        if(type == "origin"){
            OriginLabel.text = "\(item["name"])"
        }else if(type == "destination"){
            DestinationLabel.text = "\(item["name"])"
        }else if(type == "departure"){
            DepartureLabel.text = "\(item["name"])"
        }
    }
}

extension TravelViewController : DatePickerLauncherDelegate {
    func didSelectedLauncher(item: Date) {
        DateLabel.text = item.convertToView()
    }
}
