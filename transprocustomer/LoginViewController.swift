//
//  LoginViewController.swift
//  TransPro
//
//  Created by Hirito48 on 2/12/17.
//  Copyright © 2017 Hirito48. All rights reserved.
//

import UIKit
import SCLAlertView
import SwiftyJSON
import Alamofire

class LoginViewController: UIViewController {

    @IBOutlet weak var UserNameTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(Authenticator().isLogin()){
            if(Authenticator().isTravel()){
                performSegue(withIdentifier: "LoginToMainTravel", sender: self)
            }else{
                performSegue(withIdentifier: "LoginToMain", sender: self)
            }
        }
    }
    
    @IBAction func RememberButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "LoginToRemember", sender: sender)
    }
    
    @IBAction func SignInButtonTapped(_ sender: Any) {
        let url: String = "\(Config.Url.WebService)user/session"
        let myBody : String = "{ \"email\":\"\(UserNameTextField.text!)\", \"password\":\"\(PasswordTextField.text!)\"}"
        Alamofire.request(URL(string : url)!, method: .post, parameters: [:], encoding: myBody, headers: Config.Header.Guest)
            .responseJSON{ response in
                switch response.result{
                case .success(let value) :
                    let data = JSON(value)
                    print(data)
                    if(data["error"].exists()){
                        var message = ""
                        if(data["error"]["context"]["email"].exists()){
                            message = "\(data["error"]["context"]["email"][0])"
                        }else if(data["error"]["context"]["password"].exists()){
                            message = "\(data["error"]["context"]["password"][0])"
                        }else if(data["error"]["message"].exists()){
                            message = "\(data["error"]["message"])"
                        }
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: true
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.showInfo("", subTitle: message)
                    }else{
                        UserDefaults.standard.set("\(data["email"])", forKey: "email")
                        UserDefaults.standard.set("\(data["id"])", forKey: "user_id")
                        UserDefaults.standard.set("\(data["session_token"])", forKey: "token")
                        
                        UserDefaults.standard.synchronize()
                        self.performSegue(withIdentifier: "LoginToMain", sender: nil)
                    }
                case .failure(_) :
                    self.SignInButtonTapped(sender)
                }
        }
    }
    @IBAction func BackButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
