//
//  BookingViewController.swift
//  TransPro
//
//  Created by Hirito48 on 2/14/17.
//  Copyright © 2017 Hirito48. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import AlamofireNetworkActivityIndicator
import SwiftyJSON

class BookingViewController: UIViewController {
    
    @IBOutlet weak var colView: UICollectionView!
    var resource : JSON = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colView.delegate = self
        colView.dataSource = self
        loadData()
    }
    
    @IBAction func BackButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    func loadData(){
        print("asd")
        let headers : HTTPHeaders = [
            "X-DreamFactory-Api-Key" : Config.Key.Api,
            "Content-Type" : "application/json",
            "Accept" : "application/json",
            "X-DreamFactory-Session-Token" : UserDefaults.standard.string(forKey: "token")!
        ]
        let url: String = "\(Config.Url.WebService)transpro/_table/booking_detail_rent"
        Alamofire.request(URL(string : url)!, method: .get, encoding: URLEncoding.default, headers: headers)
            .responseJSON{ response in
                switch response.result{
                case .success(let value) :
                    let data = JSON(value)["resource"]
                    print(data)
                    self.resource = data
                    self.setupCollection()
                    self.colView.reloadData()
                    self.loadData2()
                case .failure( _) :
                    self.loadData()
                }
        }
    }
    func loadData2(){
        let headers : HTTPHeaders = [
            "X-DreamFactory-Api-Key" : Config.Key.Api,
            "Content-Type" : "application/json",
            "Accept" : "application/json",
            "X-DreamFactory-Session-Token" : UserDefaults.standard.string(forKey: "token")!
        ]
        let url: String = "\(Config.Url.WebService)transpro/_table/booking_detail_trip"
        Alamofire.request(URL(string : url)!, method: .get, encoding: URLEncoding.default, headers: headers)
            .responseJSON{ response in
                switch response.result{
                case .success(let value) :
                    let data = JSON(value)["resource"]
                    print(data)
                    for (_, item) in data {
                        self.resource.appendIfArray(json: item)
                    }
                    self.colView.reloadData()
                    self.loadData3()
                case .failure( _) :
                    self.loadData2()
                }
        }
    }
    func loadData3(){
        let headers : HTTPHeaders = [
            "X-DreamFactory-Api-Key" : Config.Key.Api,
            "Content-Type" : "application/json",
            "Accept" : "application/json",
            "X-DreamFactory-Session-Token" : UserDefaults.standard.string(forKey: "token")!
        ]
        let url: String = "\(Config.Url.WebService)transpro/_table/booking_detail_travel"
        Alamofire.request(URL(string : url)!, method: .get, encoding: URLEncoding.default, headers: headers)
            .responseJSON{ response in
                switch response.result{
                case .success(let value) :
                    let data = JSON(value)["resource"]
                    print(data)
                    for (_, item) in data {
                        self.resource.appendIfArray(json: item)
                    }
                    self.colView.reloadData()
                case .failure( _) :
                    self.loadData3()
                }
        }
    }
    
    
    func setupCollection(){
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: UIScreen.main.bounds.size.width, height: 125)
        layout.minimumInteritemSpacing = 1
        layout.minimumLineSpacing = 1
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        colView.collectionViewLayout = layout
    }
}

extension BookingViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookingCell", for: indexPath) as! BookingCollectionViewCell
        let data = resource[indexPath.row]
        cell.DateLabel.text = "\(data["input_date"])"
        if "\(data["category"])" == "travel" {
            cell.TitleLabel.text = "\(data["kota_awal"]) - \(data["kota_tujuan"])"
        }else{
            cell.TitleLabel.text = "\(data["product"])"
        }
        cell.CompanyLabel.text = "\(data["partner"])"
        cell.PriceLabel.text = "\(data["amount"])"
        cell.BookingImage.af_setImage(withURL: URL(string: "\(Config.Url.ImageService)\(data["image"])")!, placeholderImage: Config.Default.Image)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = resource[indexPath.row]
    }
}
