//
//  PromoViewController.swift
//  TransPro
//
//  Created by Hirito48 on 2/12/17.
//  Copyright © 2017 Hirito48. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON

class PromoViewController: UIViewController {
    @IBOutlet weak var PromoCollectionView: UICollectionView!
    
    var resource : JSON = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDataList()
    }
    
    @IBAction func BackButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func loadDataList(){
        let headers : HTTPHeaders = [
            "X-DreamFactory-Api-Key" : Config.Key.Api,
            "Content-Type" : "application/json",
            "Accept" : "application/json",
            "X-DreamFactory-Session-Token" : UserDefaults.standard.string(forKey: "token")!
        ]
        let url: String = "\(Config.Url.WebService)transpro/_table/promo"
        Alamofire.request(URL(string : url)!, method: .get, encoding: URLEncoding.default, headers: headers)
            .responseJSON{ response in
                switch response.result{
                case .success(let value) :
                    self.resource = JSON(value)["resource"]
                    self.PromoCollectionView.delegate = self
                    self.PromoCollectionView.dataSource = self
                case .failure(let error) :
                    print(error)
                    self.loadDataList()
                }
        }
    }
    
}

extension PromoViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resource.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PromoCell", for: indexPath) as! PromoCollectionViewCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = UIScreen.main.bounds.width - 36
        var h : CGFloat = 86
        h += (w/29) * 16
        return CGSize(width: w, height: h)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, 18, 8, 18)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let data = resource[indexPath.row]
        let itemCell = cell as! PromoCollectionViewCell
        itemCell.CodeLabel.text = "\(data["code_promo"])"
        itemCell.TitleLabel.text = "\(data["title"])"
        itemCell.PeriodLabel.text = "\(data["start_date"])"
        itemCell.PromoImage.af_setImage(withURL: URL(string: "\(Config.Url.ImageService)\(data["image"])")!, placeholderImage: Config.Default.Image)
    }
}

