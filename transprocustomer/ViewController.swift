//
//  ViewController.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 5/14/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import SwiftyJSON
import SnapKit
import Alamofire
import FirebaseCrash
import Firebase

class ViewController: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate {

    @IBOutlet weak var BackgroundImage: UIImageView!
    
    let loginManager = FBSDKLoginManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BackgroundImage.addBlurEffect()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().disconnect()
        self.loginManager.logOut()
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
            //            loader?.removeFromSuperview()
            return
        }
        var data : [String : String] = [:]
        data["display_name"] = user.profile.name!
        data["first_name"] = user.profile.givenName!
        data["last_name"] = user.profile.familyName!
        data["email"] = user.profile.email!
        data["hp"] = ""
        data["type"] = "google"

        sendUser(user : data)
    }

    @IBAction func RegisterButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "ViewToRegister", sender: nil)
    }
    @IBAction func SignInButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "ViewToLogin", sender: nil)
    }
    
    @IBAction func FBButtonTapped(_ sender: Any) {
        let login = FBSDKLoginManager()
        //loader
        login.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.retrieveFBData()
                    }
                }
                //loader remove
            }
        }
    }
    
    func retrieveFBData(){
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "name, first_name, last_name, email, picture, hometown, location"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if ((error) != nil){
                self.FBButtonTapped(self)
            } else {
                let user = JSON(result!)
                print(user)
                var data : [String : String] = [:]
                data["display_name"] = "\(user["name"])"
                data["first_name"] = "\(user["first_name"])"
                data["last_name"] = "\(user["last_name"])"
                data["email"] = "\(user["email"])"
                data["hp"] = ""
                data["type"] = "facebook"
                self.sendUser(user : data)
            }
        })
    }
    
    @IBAction func GoogleButtonTapped(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sendUser(user : [String : String]){
        let password : String = "\(user["email"]!)"
//        let token = FIRInstanceID.instanceID().token()!
        let url: String = "\(Config.Url.WebService)transpro/_table/customer"
        let myBody : String = "{ \"resource\" : [{ \"email\":\"\(user["email"]!)\", \"first_name\":\"\(user["first_name"]!)\", \"last_name\":\"\(user["last_name"]!)\", \"display_name\":\"\(user["display_name"]!)\", \"password\":\"\(password.aesEncrypt())\", \"type\":\"\(user["type"]!)\", \"hp\":\"\(user["hp"]!)\"}]}"
        print(url)
        Alamofire.request(URL(string : url)!, method: .post, parameters: [:], encoding: myBody, headers: Config.Header.Guest)
            .responseJSON{ response in
                switch response.result{
                case .success(let value) :
                    //                    self.loader?.removeFromSuperview()
                    let data = JSON(value)
                    print(data)
                    if(data["error"].exists()){
                        //                        alert
                        GIDSignIn.sharedInstance().disconnect()
                        self.loginManager.logOut()
                    }else{
                        let info = data["token_info"]
                        UserDefaults.standard.set("\(info["email"])", forKey: "email")
                        UserDefaults.standard.set("\(info["id"])", forKey: "user_id")
                        UserDefaults.standard.set("\(info["session_token"])", forKey: "token")
                        
                        UserDefaults.standard.synchronize()
                        self.performSegue(withIdentifier: "ViewToMain", sender: nil)
                    }
                case .failure(let error) :
                    print(error)
                    self.sendUser(user: user)
                }
        }
    }
}

