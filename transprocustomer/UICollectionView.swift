//
//  UICollectionView.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 6/21/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
    func deselectAllItems(animated: Bool = false) {
        for indexPath in self.indexPathsForSelectedItems ?? [] {
            self.deselectItem(at: indexPath, animated: animated)
        }
    }
}
