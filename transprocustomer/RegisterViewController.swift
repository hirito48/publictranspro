//
//  RegisterViewController.swift
//  TransPro
//
//  Created by Hirito48 on 2/12/17.
//  Copyright © 2017 Hirito48. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import SwiftyJSON

class RegisterViewController: UIViewController {

    @IBOutlet weak var LastNameTextField: UITextField!
    @IBOutlet weak var FirstNameTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var HpTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func Back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func SubmitButtonTapped(_ sender: Any) {
        let url: String = "\(Config.Url.WebService)user/register?login=true"
        let myBody : String = "{ \"email\":\"\(EmailTextField.text!)\", \"first_name\":\"\(FirstNameTextField.text!)\", \"last_name\":\"\(LastNameTextField.text!)\", \"display_name\":\"\(FirstNameTextField.text!) \(LastNameTextField.text!)\", \"password\":\"\(PasswordTextField.text!)\", \"type\":\"system\", \"hp\":\"\(HpTextField.text!)\"}"
        Alamofire.request(URL(string : url)!, method: .post, parameters: [:], encoding: myBody, headers: Config.Header.Guest)
            .responseJSON{ response in
                switch response.result{
                case .success(let value) :
                    let data = JSON(value)
                    print(data)
                    if(data["error"].exists()){
                        var message = ""
                        if(data["error"]["context"]["email"].exists()){
                            message = "\(data["error"]["context"]["email"][0])"
                        }else if(data["error"]["context"]["password"].exists()){
                            message = "\(data["error"]["context"]["password"][0])"
                        }else if(data["error"]["message"].exists()){
                            message = "\(data["error"]["message"])"
                        }
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: true
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.showInfo("", subTitle: message)
                    }else{
                        UserDefaults.standard.set("\(data["email"])", forKey: "email")
                        UserDefaults.standard.set("\(data["id"])", forKey: "user_id")
                        UserDefaults.standard.set("\(data["session_token"])", forKey: "token")
                        
                        UserDefaults.standard.synchronize()
                        self.performSegue(withIdentifier: "RegisterToMain", sender: nil)
                    }
                case .failure(_) :
                    self.SubmitButtonTapped(sender)
                }
        }
    }
    
}
