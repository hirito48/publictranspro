//
//  CalendarViewController.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 5/27/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import UIKit
import JTAppleCalendar
import AnimatedCollectionViewLayout

class CalendarViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var CalendarView: JTAppleCalendarView!
    @IBOutlet weak var MonthView: UICollectionView!
    
    let formatter = DateFormatter()
    var selectedDate: Date?
    var inDate: Date?
    var outDate: Date?
    var isLoaded = false
    
    var blue = UIColor.init(hexString: "#4dc7f3")
    var lightBlue = UIColor.init(hexString: "#b2e7f9")
    var textCalendar = UIColor.init(hexString: "#353535")
    var isMultiple = false
    var isUpdated = true
    
    var months = ["Januari", "Februari", "Maret"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCalendar()
        setupMonth()
    }

    func setupCalendar(){
        CalendarView.minimumLineSpacing = 0
        CalendarView.minimumInteritemSpacing = 0
        CalendarView.scrollDirection = .horizontal
        CalendarView.isPagingEnabled = true
        CalendarView.isRangeSelectionUsed = isMultiple
        CalendarView.allowsMultipleSelection = isMultiple
    }
    
    func setupMonth(){
        MonthView.delegate = self
        MonthView.dataSource = self
        let layout = AnimatedCollectionViewLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.animator = CubeAttributesAnimator()
        MonthView.collectionViewLayout = layout
    }
    
    @IBAction func BackButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func next() {
        CalendarView.scrollToSegment(.next)
    }
    
    func previous() {
        CalendarView.scrollToSegment(.previous)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return months.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MonthCell", for: indexPath) as! MonthCollectionViewCell
        cell.MonthLabel.text = "asd"
        return cell
    }
}

extension CalendarViewController : JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {

    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        let startDate = formatter.date(from : "2017 01 01")
        let endDate = formatter.date(from : "2017 12 31")
        
        let parameters = ConfigurationParameters(startDate: startDate!, endDate: endDate!, firstDayOfWeek: .monday)
        
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CalendarCell", for: indexPath) as! CalendarCollectionViewCell
        cell.DayLabel.text = cellState.text
        cell.Button.accessibilityHint = formatter.string(from: cellState.date)
        cell.Button.addTarget(self, action: #selector(handleTappedCalendar), for: .touchUpInside)
        handleCellSelection(view: cell, cellState: cellState
            )
        return cell
    }
    
    func handleTappedCalendar(sender : UIButton){
        let x = formatter.date(from: sender.accessibilityHint!)
        CalendarView.deselectAllDates()
        if inDate != nil {
            CalendarView.selectDates(from: inDate!, to: x!)
            inDate = x
        } else {
            inDate = x
        }
    }
    
    func handleCellSelection(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? CalendarCollectionViewCell else {return }
        switch cellState.selectedPosition() {
        case .left, .right:
            myCustomCell.backgroundColor = blue
            myCustomCell.DayLabel.textColor = .white
        case .middle:
            myCustomCell.backgroundColor = lightBlue
            myCustomCell.DayLabel.textColor = .white
        default:
            myCustomCell.backgroundColor = .white
            if cellState.dateBelongsTo == .thisMonth {
                myCustomCell.DayLabel.textColor = textCalendar
            } else {
                myCustomCell.DayLabel.textColor = .gray
            }
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellSelection(view: cell, cellState: cellState)
    }
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        handleCellSelection(view: cell, cellState: cellState)
    }
    
}
