//
//  TripViewController.swift
//  TransPro
//
//  Created by Hirito48 on 2/12/17.
//  Copyright © 2017 Hirito48. All rights reserved.
//

import UIKit

class TripViewController: UIViewController {
    @IBOutlet weak var DateButton: UIButton!
    @IBOutlet weak var SearchTextField: UITextField!
    @IBOutlet weak var DateLabel: UILabel!
    
    var DatePicker = DatePickerLauncher()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DatePicker.delegate = self
        DateButton.addTarget(DatePicker, action: #selector(DatePicker.handlePresent), for: .touchUpInside)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        DateButton.layer.addBorder(edge: .bottom, color: Helper().TransProColor(color : "Black", alpha: 0.4), thickness: 0.5)
        SearchTextField.layer.addBorder(edge: .bottom, color: Helper().TransProColor(color : "Black", alpha: 0.4), thickness: 0.5)
        
    }
    
    @IBAction func BackButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func NextButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "TripToResult", sender: sender)
    }

}

extension TripViewController : DatePickerLauncherDelegate{
    func didSelectedLauncher(item: Date) {
        DateLabel.text = item.convertToView()
    }
}
