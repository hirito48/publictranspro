//
//  BaseCell.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 5/27/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import Foundation
import UIKit

class BaseCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setupViews() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
