//
//  MessagesViewController.swift
//  TransPro
//
//  Created by Hirito48 on 2/14/17.
//  Copyright © 2017 Hirito48. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MessagesViewController: UIViewController {
    @IBOutlet weak var CollectionView: UICollectionView!

    var resource : JSON = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadDataList()
    }
    
    @IBAction func BackButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    func loadDataList(){
        let headers : HTTPHeaders = [
        "X-DreamFactory-Api-Key" : Config.Key.Api,
        "Content-Type" : "application/json",
        "Accept" : "application/json",
        "X-DreamFactory-Session-Token" : UserDefaults.standard.string(forKey: "token")!
        ]
        let url: String = "\(Config.Url.WebService)transpro/_table/messages?filter=(receiver%20%3D%20\(UserDefaults.standard.string(forKey: "user_id")!))%20and%20(type%20%3D%201)"
        Alamofire.request(URL(string : url)!, method: .get, encoding: URLEncoding.default, headers: headers)
            .responseJSON{ response in
                switch response.result{
                case .success(let value) :
                    self.resource = JSON(value)["resource"]
                    self.CollectionView.delegate = self
                    self.CollectionView.dataSource = self
                case .failure(_) :
                    self.loadDataList()
                }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MessageToNew" {
            let input = sender as! JSON
            let temp : [String : String] = ["id" : input["sender"].string!, "name" : input["sender"].string!]
            let dest = segue.destination as! NewMessagesViewController
            dest.data = temp
        }
    }
}

extension MessagesViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resource.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MessagesCell", for: indexPath) as! MessageCollectionViewCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let data = resource[indexPath.row]
        print(data)
        let itemCell = cell as! MessageCollectionViewCell
        itemCell.AuthorLabel.setTitle("\(resource[indexPath.row]["sender"])", for: .normal)
        itemCell.ContentLabel.text = "\(resource[indexPath.row]["message"])"
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = "\(resource[indexPath.row]["message"])".height(width: UIScreen.main.bounds.width-16, font: UIFont.systemFont(ofSize: 12)) + 30 + 12 + 4
        return CGSize(width: UIScreen.main.bounds.width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "MessageToNew", sender: resource[indexPath.row])
    }
}
