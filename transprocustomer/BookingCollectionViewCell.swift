//
//  BookingCollectionViewCell.swift
//  TransPro
//
//  Created by Hirito48 on 2/16/17.
//  Copyright © 2017 Hirito48. All rights reserved.
//

import UIKit

class BookingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var BookingImage: UIImageView!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var CompanyLabel: UILabel!
    @IBOutlet weak var StatusLabel: UILabel!
    @IBOutlet weak var PriceLabel: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
}
