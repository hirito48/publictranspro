//
//  CalendarCollectionViewCell.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 5/27/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarCollectionViewCell: JTAppleCell {
    
    @IBOutlet weak var Button: UIButton!
    @IBOutlet weak var DayLabel: UILabel!
}
