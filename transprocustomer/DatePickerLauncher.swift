//
//  DatePickerLauncher.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 7/11/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import Foundation
import UIKit

protocol DatePickerLauncherDelegate {
    func didSelectedLauncher(item : Date)
}

class DatePickerLauncher : NSObject {
    let blackView = UIView()
    var picker = UIDatePicker()
    let btnSubmit = UIButton()
    var delegate : DatePickerLauncherDelegate?
    
    init(type : String = "city") {
        super.init()
        picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.setValue(Helper().TransProColor(color: "BluePrimary"), forKeyPath: "textColor")
    }
    
    func selectAct(){
        handleDismiss()
        delegate?.didSelectedLauncher(item: picker.date)
    }
    
    func handlePresent() {
        picker.backgroundColor = .white
        if let window = UIApplication.shared.keyWindow {
            
            btnSubmit.addTarget(self, action: #selector(selectAct), for: .touchUpInside)
            
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleDismiss)))
            
            window.addSubview(blackView)
            
            window.addSubview(picker)
            let width : CGFloat = UIScreen.main.bounds.width
            let height: CGFloat = 0.3 * width
            
            picker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height + height, width: width, height: height)
            
            blackView.frame = window.frame
            blackView.alpha = 0
            
            btnSubmit.frame.size = CGSize(width: UIScreen.main.bounds.width, height: 45)
            btnSubmit.frame.origin = CGPoint(x: 0, y: UIScreen.main.bounds.height + 45)
            btnSubmit.setTitle("Pilih", for: .normal)
            btnSubmit.backgroundColor = Helper().TransProColor(color: "BlueSecondary")
            btnSubmit.setTitleColor(.white, for: .normal)
            blackView.addSubview(btnSubmit)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.picker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - height - 45, width: self.picker.frame.width, height: self.picker.frame.height)
                self.btnSubmit.frame.origin = CGPoint(x: 0, y: UIScreen.main.bounds.height - 45)
            }, completion: nil)
        }
    }
    
    func handleDismiss() {
        UIView.animate(withDuration: 0.5, delay: 0.5, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackView.alpha = 0
            
            if let window = UIApplication.shared.keyWindow {
                self.picker.frame.origin = CGPoint(x: 0, y: window.frame.height + self.picker.frame.height)
                
                self.btnSubmit.frame.origin = CGPoint(x: 0, y: UIScreen.main.bounds.height + 45)
            }
            
        })
    }
}
