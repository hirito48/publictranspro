//
//  DurationPickerView.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 7/10/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import UIKit

class DurationPickerView: BasePickerView {
    
    override func setupView() {
        super.setupView()
        delegate = self
        dataSource = self
        backgroundColor = .white
        resource = [["name" : "1 Jam", "value" : "1"], ["name" : "3 Jam", "value" : "3"], ["name" : "6 Jam", "value" : "6"], ["name" : "12 Jam", "value" : "12"]]
        reloadAllComponents()
    }
}

extension DurationPickerView : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return resource.count
    }
}

extension DurationPickerView : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selected = resource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.textColor = Helper().TransProColor(color: "BluePrimary")
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "\(resource[row]["name"])"
        
        return label
    }
    
}
