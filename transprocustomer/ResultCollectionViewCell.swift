//
//  ResultCollectionViewCell.swift
//  TransPro
//
//  Created by Hirito48 on 2/12/17.
//  Copyright © 2017 Hirito48. All rights reserved.
//

import UIKit

class ResultCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var CompanyLabel: UILabel!
    @IBOutlet weak var ResultImage: UIImageView!
    @IBOutlet weak var PriceLabel: UILabel!
    
}
