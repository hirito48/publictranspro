//
//  MainViewController.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 5/14/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import Material

class MainViewController: UIViewController, UIScrollViewDelegate {
    
    
    @IBOutlet weak var HeightSlider: NSLayoutConstraint!
    @IBOutlet weak var ImagesSlider: UIScrollView!
    fileprivate var buttons = [Button]()
    fileprivate var tabBar: TabBar!
    
    @IBOutlet weak var BookingButton: UIButton!
    @IBOutlet weak var MessageButton: UIButton!
    @IBOutlet weak var ProfileButton: UIButton!
    
    @IBOutlet weak var ButtonContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mainWidth = self.view.frame.size.width
        ImagesSlider.frame.size.width = mainWidth
        HeightSlider.constant = mainWidth/2
        ImagesSlider.isPagingEnabled = true
        ImagesSlider.showsHorizontalScrollIndicator = false
        ImagesSlider.delegate = self
        setupLoader()
        setupButtons()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupButtons(){
        BookingButton.addTarget(self, action: #selector(bookingButtonTapped), for: .touchUpInside)
        ProfileButton.addTarget(self, action: #selector(profileButtonTapped), for: .touchUpInside)
        MessageButton.addTarget(self, action: #selector(messageButtonTapped), for: .touchUpInside)
        
        let buttons : [UIButton] = [BookingButton, MessageButton, ProfileButton]
        let widthBtn = UIScreen.main.bounds.width/3
        var counter : CGFloat = 0
        for button in buttons{
            button.frame = CGRect(x: counter*widthBtn, y: 0, width: widthBtn, height: 45.0)
            counter += 1
        }
        ButtonContainer.frame.size = CGSize(width : UIScreen.main.bounds.width, height : 45.0)
        ButtonContainer.layer.addBorder(edge: .top, color: UIColor.lightGray, thickness: 1)
    }
    
    func bookingButtonTapped(){
        performSegue(withIdentifier: "MainToBooking", sender: nil)
    }
    
    func messageButtonTapped(){
        performSegue(withIdentifier: "MainToMessages", sender: nil)
    }
    
    func profileButtonTapped(){
        performSegue(withIdentifier: "MainToProfile", sender: nil)
    }
    
    func setupLoader(){
        let headers : HTTPHeaders = [
            "X-DreamFactory-Api-Key" : Config.Key.Api,
            "Content-Type" : "application/json",
            "Accept" : "application/json",
            "X-DreamFactory-Session-Token" : UserDefaults.standard.string(forKey: "token")!
        ]
        let url: String = "\(Config.Url.WebService)transpro/_table/promo?filter=featured=true&limit=5&order=input_date%20DESC"
        Alamofire.request(URL(string : url)!, method: .get, encoding: URLEncoding.default, headers: headers)
            .responseJSON{ response in
                switch response.result{
                case .success(let value) :
                    let data = JSON(value)
                    if data["resource"].exists() {
                        self.loadSlider(sliderArray : data["resource"])
                    } else {
                        self.performSegue(withIdentifier: "MainToLogin", sender: nil)
                    }
                case .failure( _) :
                    self.setupLoader()
                }
        }
    }
    
    func loadSlider(sliderArray : JSON){
        for (index, slide) in sliderArray{
            if let slider = Bundle.main.loadNibNamed("Slider", owner: self, options: nil)?.first as? SliderView {
                slider.ImageSlider.af_setImage(withURL: URL(string: "\(Config.Url.ImageService)\(slide["image"])")!, placeholderImage: Config.Default.Image)
                slider.ImageSlider.contentMode = .scaleAspectFill
                slider.frame.size.width = self.view.bounds.size.width
                slider.frame.size.height = ImagesSlider.frame.size.height
                slider.frame.origin.x = CGFloat(Int(index)!) * self.view.bounds.size.width
                ImagesSlider.addSubview(slider)
                ImagesSlider.contentSize.width = ImagesSlider.frame.width * CGFloat(Int(index)! + 1)
            }
        }
    }
    
    @IBAction func RentButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "MainToRent", sender: sender)
    }
    @IBAction func TripButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "MainToTrip", sender: sender)
    }
    @IBAction func TravelButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "MainToTravel", sender: sender)
    }
    @IBAction func PromoButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "MainToPromo", sender: sender)
    }
    
}
