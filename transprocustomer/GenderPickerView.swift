//
//  GenderPickerView.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 7/12/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import Foundation
import UIKit

class GenderPickerView: BasePickerView {
    
    override func setupView() {
        super.setupView()
        delegate = self
        dataSource = self
        backgroundColor = .white
        resource = [["name" : "Laki - laki", "value" : "laki-laki"], ["name" : "Perempuan", "value" : "perempuan"]]
        reloadAllComponents()
    }
}

extension GenderPickerView : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return resource.count
    }
}

extension GenderPickerView : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selected = resource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.textColor = Helper().TransProColor(color: "BluePrimary")
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "\(resource[row]["name"])"
        
        return label
    }
    
}
