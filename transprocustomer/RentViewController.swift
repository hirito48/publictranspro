
//  RentViewController.swift
//  TransPro
//
//  Created by Hirito48 on 2/12/17.
//  Copyright © 2017 Hirito48. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class RentViewController: UIViewController {

    @IBOutlet weak var TypeLabel: UILabel!
    @IBOutlet weak var DurationLabel: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    @IBOutlet weak var LocationLabel: UILabel!
    @IBOutlet weak var TypeButton: UIButton!
    @IBOutlet weak var DurationButton: UIButton!
    @IBOutlet weak var DateButton: UIButton!
    @IBOutlet weak var LocationButton: UIButton!
    @IBOutlet weak var TypeSwitch: UISwitch!
    
    var data = [String : String]()
    var dropDownType = DropDown()
    var CityPicker = Launcher()
    var DurationPicker = Launcher(type: "duration")
    var TypeCarPicker = Launcher(type: "type_car")
    var DatePicker = DatePickerLauncher()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        LocationButton.addTarget(CityPicker, action: #selector(CityPicker.handlePresent), for: .touchUpInside)
        DurationButton.addTarget(DurationPicker, action: #selector(DurationPicker.handlePresent), for: .touchUpInside)
        TypeButton.addTarget(TypeCarPicker, action: #selector(TypeCarPicker.handlePresent), for: .touchUpInside)
        DateButton.addTarget(DatePicker, action: #selector(DatePicker.handlePresent), for: .touchUpInside)
        CityPicker.delegate = self
        DurationPicker.delegate = self
        TypeCarPicker.delegate = self
        DatePicker.delegate = self
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RentToLocation" {
            
        }else if segue.identifier == "RentToCalendar" {
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        LocationButton.layer.addBorder(edge: .bottom, color: Helper().TransProColor(color : "Black", alpha: 0.4), thickness: 0.5)
        DurationButton.layer.addBorder(edge: .bottom, color: Helper().TransProColor(color : "Black", alpha: 0.4), thickness: 0.5)
        DateButton.layer.addBorder(edge: .bottom, color: Helper().TransProColor(color : "Black", alpha: 0.4), thickness: 0.5)
        TypeButton.layer.addBorder(edge: .bottom, color: Helper().TransProColor(color : "Black", alpha: 0.4), thickness: 0.5)
    }
    
    func openCalendar(){
        performSegue(withIdentifier: "RentToCalendar", sender: nil)
    }
    
    @IBAction func BackButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func SubmitButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "RentToResult", sender: sender)
    }
    
}

extension RentViewController : LauncherDelegate {
    func didSelectedLauncher(type: String, item: JSON) {
        print(item)
        if(type == "city"){
            LocationLabel.text = "\(item["name"])"
        }else if(type == "duration"){
            DurationLabel.text = "\(item["name"])"
        }else if(type == "type_car"){
            TypeLabel.text = "\(item["name"])"
        }
    }
}
extension RentViewController : DatePickerLauncherDelegate {
    func didSelectedLauncher(item: Date) {
        DateLabel.text = item.convertToView()
    }
}
