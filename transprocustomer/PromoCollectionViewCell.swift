//
//  PromoCollectionViewCell.swift
//  TransPro
//
//  Created by Hirito48 on 2/12/17.
//  Copyright © 2017 Hirito48. All rights reserved.
//

import UIKit

class PromoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var PromoImage: UIImageView!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var CodeLabel: UILabel!
    @IBOutlet weak var PeriodLabel: UILabel!
    
}
