//
//  ProfileViewController.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 5/15/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SCLAlertView

class ProfileViewController: UIViewController {

    @IBOutlet weak var GenderButton: UIButton!
    @IBOutlet weak var IdentityButton: UIButton!
    @IBOutlet weak var TypeIdentityTextField: UITextField!
    @IBOutlet weak var GenderTextField: UITextField!
    @IBOutlet weak var AddressTextField: TextArea!
    @IBOutlet weak var NoIdentityTextField: UITextField!
    @IBOutlet weak var HandphoneTextField: UITextField!
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var NameTextField: UITextField!
    
    var genderPicker = Launcher(type: "gender", id : "gender")
    var identityPicker = Launcher(type: "identity", id : "identity")
    var data : JSON = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddressTextField.createRounded()
        loadData()
    }
    
    func loadData(){
        let headers : HTTPHeaders = [
            "X-DreamFactory-Api-Key" : Config.Key.Api,
            "Content-Type" : "application/json",
            "Accept" : "application/json",
            "X-DreamFactory-Session-Token" : UserDefaults.standard.string(forKey: "token")!
        ]
        let url: String = "\(Config.Url.WebService)transpro/_table/customer/\(UserDefaults.standard.string(forKey: "user_id")!)"
        Alamofire.request(URL(string : url)!, method: .get, encoding: URLEncoding.default, headers: headers)
            .responseJSON{ response in
                switch response.result{
                case .success(let value) :
                    self.data = JSON(value)
                    self.setupView()
                case .failure(let error) :
                    print(error)
                    self.loadData()
                }
        }
    }
    
    func setupView(){
        NameTextField.text = "\(data["name"])".formatting()
        EmailTextField.text = "\(data["email"])".formatting()
        HandphoneTextField.text = "\(data["telp"])".formatting()
        if data["gender"] == "l" {
            GenderTextField.text = "Laki - laki"
        } else {
            GenderTextField.text = "Perempuan"
        }
        TypeIdentityTextField.text = "\(data["type_identity"])".uppercased()
        NoIdentityTextField.text = "\(data["no_identity"])".formatting()
        AddressTextField.text = "\(data["address"])".formatting()
    }

    @IBAction func BackButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func LogoutButtonTapped(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "user_id")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.synchronize()
        performSegue(withIdentifier: "ProfileToLogin", sender: nil)
    }
    
    @IBAction func SaveButtonTapped(_ sender: Any) {
        let url: String = "\(Config.Url.WebService)transpro/_table/customer/\(UserDefaults.standard.string(forKey: "user_id")!)"
        let myBody : String = "{ \"name\":\"\(NameTextField.text!)\", \"telp\":\"\(HandphoneTextField.text!)\", \"gender\":\"\(GenderTextField.text!)\", \"type_identity\":\"\(TypeIdentityTextField.text!)\", \"no_identity\":\"\(NoIdentityTextField.text!)\", \"address\":\"\(AddressTextField.text!)\" }"
        let headers : HTTPHeaders = [
            "X-DreamFactory-Api-Key" : Config.Key.Api,
            "Content-Type" : "application/json",
            "Accept" : "application/json",
            "X-DreamFactory-Session-Token" : UserDefaults.standard.string(forKey: "token")!
        ]
        Alamofire.request(URL(string : url)!, method: .post, parameters: [:], encoding: myBody, headers: headers)
            .responseJSON{ response in
                switch response.result{
                case .success(let value) :
                    let data = JSON(value)
                    print(data)
                    if(data["error"]["message"].exists()){
                        let message = "\(data["error"]["message"])".formatting()
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: true
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.showInfo("", subTitle: message)
                    }else{
                        let message = "Perubahan telah disimpan."
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: true
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.showInfo("", subTitle: message)
                    }
                case .failure(_) :
                    self.SaveButtonTapped(sender)
                }
        }
    }
    
}
