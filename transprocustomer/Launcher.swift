//
//  Launcher.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 7/9/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

protocol LauncherDelegate {
    func didSelectedLauncher(type : String, item : JSON)
}

class Launcher : NSObject {
    let blackView = UIView()
    var picker = BasePickerView()
    let btnSubmit = UIButton()
    var delegate : LauncherDelegate?
    var id : String = ""
    
    init(type : String = "city", id : String = "") {
        super.init()
        if type == "city" {
            picker = CityPickerView()
        } else if type == "type_car" {
            picker = TypePickerView()
        } else if type == "duration" {
            picker = DurationPickerView()
        } else if type == "departure" {
            picker = DeparturePickerView()
        } else {
            
        }
        
        if id != "" {
            self.id = id
        }else{
            self.id = type
        }
    }
    
    func selectAct(){
        let value = picker.resource[picker.selectedRow(inComponent: 0)]
        handleDismiss()
        delegate?.didSelectedLauncher(type : id, item : value)
    }
    
    func handlePresent() {
        picker.backgroundColor = .white
        if let window = UIApplication.shared.keyWindow {
            
            btnSubmit.addTarget(self, action: #selector(selectAct), for: .touchUpInside)
            
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleDismiss)))
            
            window.addSubview(blackView)
            
            window.addSubview(picker)
            let width : CGFloat = UIScreen.main.bounds.width
            let height: CGFloat = 0.3 * width
            
            picker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height + height, width: width, height: height)
            
            blackView.frame = window.frame
            blackView.alpha = 0
            
            btnSubmit.frame.size = CGSize(width: UIScreen.main.bounds.width, height: 45)
            btnSubmit.frame.origin = CGPoint(x: 0, y: UIScreen.main.bounds.height + 45)
            btnSubmit.setTitle("Pilih", for: .normal)
            btnSubmit.backgroundColor = Helper().TransProColor(color: "BlueSecondary")
            btnSubmit.setTitleColor(.white, for: .normal)
            blackView.addSubview(btnSubmit)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.picker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - height - 45, width: self.picker.frame.width, height: self.picker.frame.height)
                self.btnSubmit.frame.origin = CGPoint(x: 0, y: UIScreen.main.bounds.height - 45)
            }, completion: nil)
        }
    }
    
    func handleDismiss() {
        UIView.animate(withDuration: 0.5, delay: 0.5, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackView.alpha = 0
            
            if let window = UIApplication.shared.keyWindow {
                self.picker.frame.origin = CGPoint(x: 0, y: window.frame.height + self.picker.frame.height)
                
                self.btnSubmit.frame.origin = CGPoint(x: 0, y: UIScreen.main.bounds.height + 45)
            }
            
        })
    }
    
}
