//
//  NewMessagesViewController.swift
//  TransPro
//
//  Created by Hirito48 on 2/14/17.
//  Copyright © 2017 Hirito48. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import SwiftyJSON

class NewMessagesViewController: UIViewController, UITextViewDelegate {
    @IBOutlet weak var AuthorLabel: UILabel!
    @IBOutlet weak var MessageTextView: TextArea!
    
    var data : [String:String] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        AuthorLabel.text = "Kepada : \(data["name"]!)"
    }
    

    @IBAction func BackButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "NewToMessage", sender: sender)
    }
    @IBAction func SubmitButtonTapped(_ sender: Any) {
        let url: String = "\(Config.Url.WebService)message/new"
        
        let params : Parameters = [
            "kepada" : data["id"] ?? "-",
            "pesan" : MessageTextView.text
        ]
        let headers : HTTPHeaders = [
            "X-DreamFactory-Api-Key" : Config.Key.Api,
            "Content-Type" : "application/json",
            "Accept" : "application/json",
            "X-DreamFactory-Session-Token" : UserDefaults.standard.string(forKey: "token")!
        ]
        Alamofire.request(URL(string : url)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{ response in
                switch response.result{
                case .success(let value) :
                    let data = JSON(value)
                    
                    if("\(data["response"])" == "200"){
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.addButton("Tutup", target:self, selector:#selector(self.successSubmit))
                        alertView.showInfo("", subTitle: "\(data["message"])")
                    }else{
                        SCLAlertView().showInfo("", subTitle: "\(data["message"])")
                    }
                case .failure(let error) :
                    print(error)
                    self.SubmitButtonTapped(sender)
                }
        }
    }
    
    func successSubmit(){
        performSegue(withIdentifier: "NewToMessage", sender: self)
    }
    
}
