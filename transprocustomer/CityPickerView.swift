//
//  CityPickerView.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 7/9/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class CityPickerView: BasePickerView {

    override func setupView() {
        super.setupView()
        delegate = self
        dataSource = self
        backgroundColor = .white
        
        loadData()
    }
    
    func loadData(){
        let headers : HTTPHeaders = [
            "X-DreamFactory-Api-Key" : Config.Key.Api,
            "Content-Type" : "application/json",
            "Accept" : "application/json",
            "X-DreamFactory-Session-Token" : UserDefaults.standard.string(forKey: "token")!
        ]
        let url: String = "\(Config.Url.WebService)transpro/_table/kabupaten"
        Alamofire.request(URL(string : url)!, method: .get, encoding: URLEncoding.default, headers: headers)
            .responseJSON{ response in
                switch response.result{
                case .success(let value) :
                    let data = JSON(value)["resource"]
                    self.resource = data
                    self.reloadAllComponents()
                case .failure( _) :
                    self.loadData()
                }
        }
    }
}

extension CityPickerView : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return resource.count
    }
}

extension CityPickerView : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selected = resource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.textColor = Helper().TransProColor(color: "BluePrimary")
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        
        label.text = "\(resource[row]["name"])"
        
        return label
    }

}
