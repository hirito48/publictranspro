//
//  DeparturePickerView.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 7/10/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import UIKit
import SwiftyJSON

class DeparturePickerView: BasePickerView {

    override func setupView() {
        super.setupView()
        delegate = self
        dataSource = self
        backgroundColor = .white
        resource = [["name" : "Dini Hari", "start" : "00:00:00", "end" : "05:59:00"], ["name" : "Pagi Hari", "start" : "06:00:00", "end" : "09:59:00"], ["name" : "Siang Hari", "start" : "10:00:00", "end" : "13:59:00"], ["name" : "Sore Hari", "start" : "14:00:00", "end" : "18:59:00"], ["name" : "Malam Hari", "start" : "19:00:00", "end" : "23:59:00"]]
        reloadAllComponents()
    }
}

extension DeparturePickerView : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return resource.count
    }
}

extension DeparturePickerView : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selected = resource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.textColor = Helper().TransProColor(color: "BluePrimary")
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "\(resource[row]["name"])"
        
        return label
    }
    
}
