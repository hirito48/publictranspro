//
//  ResultViewController.swift
//  TransPro
//
//  Created by Hirito48 on 2/12/17.
//  Copyright © 2017 Hirito48. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ResultViewController: UIViewController {
    
    @IBOutlet weak var ResCollectionView: UICollectionView!
    var selectedMenu : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func loadDataCollection(){
    
    }
    
    @IBAction func BackButtonTapped(_ sender: Any) {
        var segue = "ResultToMain"
        if(selectedMenu == "trip"){
            segue = "ResultToTrip"
        }else if(selectedMenu == "travel"){
            segue = "ResultToTravel"
        }else if(selectedMenu == "rent"){
            segue = "ResultToRent"
        }
        performSegue(withIdentifier: segue, sender: sender)
    }

}
