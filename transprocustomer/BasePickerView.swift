//
//  BasePickerView.swift
//  transprocustomer
//
//  Created by EL-CAPITAN on 7/9/17.
//  Copyright © 2017 Transpro. All rights reserved.
//

import UIKit
import SwiftyJSON

class BasePickerView: UIPickerView {

    var resource : JSON = []
    var selected : JSON = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    func setupView(){
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
